from selenium import webdriver
from bs4 import BeautifulSoup
# import org.openqa.selenium.JavascriptExecutor
import time
from Utils import *
from Product_Scrapper import *
import re

class Scraper:
    driver = 0
    Utils = Utils()
    soup = 0
    allArticles = []
    linksToProduct = []

    def __init__(self):
        url = "https://www.joueclub.fr/jouets-1er-age/jeux-d-eveil-bebe.html"
        chromedriverPath = '/home/wadi/Documents/Scrapping/chromedriver_linux64/chromedriver'
        self.driver = webdriver.Chrome(chromedriverPath)
        self.driver.get(url)

    def load_data(self):
        print("------------ Loading the page ------------")
        self.driver.implicitly_wait(10)
        # Commenter la ligne ci dessous permettra d'éviter de charger toutes la page et ne chargera que les premiers jouets sur la page
        self.Utils.scroll_down(self.driver)
        self.soup = BeautifulSoup(self.driver.page_source, 'lxml')

    def get_title(self):
        print("------------ Titles and References ------------")
        links = self.soup.findAll("a", {"class": "product__title product-label"})
        for link in links:
            article = link.text.split("Ref :", 1)
            title = article[0].strip()
            ref = article[1].strip()
            myarticle = {
                "Nom": title,
                "Reference": ref,
                "Price": 0,
                "Description": "",
                "Age_min": "",
                "Image": ""
            }
            self.allArticles.append(myarticle)
            self.linksToProduct.append(link['href'])

    def get_price(self):
        print("------------ Prices ------------")
        links = self.soup.findAll("span", {"class": "price-value"})
        array = []
        i = 0
        for link in links:
            price = link.text.split("€", 1)[0]
            array.append(price)
        for Article in self.allArticles:
            Article["Price"] = array[i].strip()
            i += 1

    def get_image(self):
        print("------------ Images ------------")
        links = self.soup.findAll("img", {"class": "image-format-listItem-max-size"})
        array = []
        i = 0
        for link in links:
            image = link['src']
            array.append(image)
        for Article in self.allArticles:
            Article["Image"] = array[i].strip()
            i += 1

    def quit(self):
        # print(self.linksToProduct)
        print(self.allArticles)
        self.driver.close()
        self.driver.quit()

    def get_product(self):
        product = Product_Scrapper(self.allArticles, self.linksToProduct, self.driver)
        self.allArticles = product.get_product()

    def start_scrap(self, Scraper):
        Scraper.load_data()
        Scraper.get_title()
        Scraper.get_price()
        Scraper.get_image()
        Scraper.get_product()
        Scraper.quit()

Scraper = Scraper()
Scraper.start_scrap(Scraper)

