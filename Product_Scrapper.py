from selenium import webdriver
from bs4 import BeautifulSoup

class Product_Scrapper:
    allArticles = []
    linksToProduct = []
    driver = 0

    def __init__(self, allArticles, linksToProduct, driver):
        self.allArticles = allArticles
        self.linksToProduct = linksToProduct
        self.driver = driver

    def get_product(self):
        array_age = []
        array_description = []
        i = 0
        print("------------ Age and Descriptions ------------")

        for url in self.linksToProduct:
            self.driver.get(url)
            soup = BeautifulSoup(self.driver.page_source, 'lxml')
            self.driver.implicitly_wait(10)
            array_age = self.get_age(soup, array_age)
            array_description = self.get_description(soup, array_description)

        for product in self.allArticles:
            product["Age_min"] = array_age[i]
            product["Description"] = array_description[i]
            i += 1
        return (self.allArticles)

    def get_description(self, soup, array_description):
        xpath = "/html/body/section[2]/main/div/div/article/div[3]/div[1]/div/div[1]/div/span[1]"
        if (self.driver.find_element_by_xpath(xpath).text != ""):
            self.driver.find_element_by_xpath(xpath).click()
        try:
            xpath = "/html/body/section[2]/main/div/div/article/div[3]/div[1]/div/div[1]/div/div/div[2]/div/div/div/p"
            description = self.driver.find_element_by_xpath(xpath).text
            array_description.append(description)
        except:
            pass

        return (array_description)

    def get_age(self, soup, array_age):
        xpath = "/html/body/section[2]/main/div/div/article/div[2]/div/div[2]/div/div[2]/div[1]/div/p/span"
        try:
            age = self.driver.find_element_by_xpath(xpath).text
        except:
            age = "Pas d'âge minimum"
        array_age.append(age)
        return (array_age)
